Lí do chọn đề tài:
- Siêu thị là nơi trao đổi lượng hàng hóa lớn
- Số lượng khách hang và nhà cung cấp lớn -> Cần nhiều nhân viên
=> Cần một phần mềm quản lí bán hàng siêu thị

Phần mềm cần quản lí 6 nghiệp vụ:
- Quản lí kho
- Quản lí khách hàng thân thiết
- Quản lí nhân viên
- Quản lí nhà cung cấp
- Quản lí hàng hóa
- Thống kê

Phần mềm được viết bằng ngôn ngữ lập trình C#

